<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    /**
     * @Route("/produit", name="produit")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($produit);
            $manager->flush();
            return $this->redirectToRoute('view_index');
        }

      
        return $this->render('produit/produit.html.twig', [
            'formulaire' => $form->createView(),
            ]);
         

    }
    /**
     * @Route("/produit/view", name="view_index")
     */
    public function view(ProduitRepository $repository){

     $produits = $repository->findAll();
     return $this->render('produit/view.html.twig', [
         'produits' => $produits
     ]);

    }
    
}
